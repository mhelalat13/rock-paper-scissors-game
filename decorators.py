from datetime import datetime
from config import score_bord


def timer(func):
    def wrap_timer(*args, **kwargs):
        time_start = datetime.now()
        result = func(*args, **kwargs)
        time_end = datetime.now()
        duration = time_end - time_start
        print(f"time of {score_bord['hands_game']} hands::"
              f" {duration.seconds // 3600}:"
              f"{duration.seconds // 60}:"
              f"{duration.seconds % 60}")
        return result

    return wrap_timer
