from random import choice
from config import GAME_CHOICES, RULE, score_bord
from decorators import timer


def get_user_choice():
    user_input = input("choice from (r, p, s):")
    if user_input not in GAME_CHOICES:
        print("Oops! your choice is not valid... try Again :")
        return get_user_choice()
    else:
        return user_input


def get_system_choice():
    return choice(GAME_CHOICES)


def check_win(user, system):
    match = {user, system}

    if len(match) == 1:
        return None

    '''
    match ro sort mikonim va tabdil be tuple mikonim 
    va be onvan kilid be RULE midahim
    '''
    return RULE[tuple(sorted(match))]


def update_table(result,):

    if result['user'] == 3:
        score_bord["user"] += 1
        msg = "You Win"
        score_bord["hands_game"] += 1
    else:
        score_bord["system"] += 1
        msg = "You Lose"
        score_bord["hands_game"] += 1

    print("#" * 30)
    print("##", f'User: {score_bord["user"]}'.ljust(24), "##")
    print("##", f'System: {score_bord["system"]}'.ljust(24), "##")
    print("##", f'Last Game: {msg}'.ljust(24), "##")
    print("#" * 30)
    print(f"hands of game: {score_bord['hands_game']}")


def play_again():
    play_again_input = input("Do you want play again? (y/N):")
    if play_again_input == 'y':
        play_one_hand()
    else:
        return None


def play_one_hand():
    result = {'user': 0, 'system': 0}
    while result['user'] < 3 and result['system'] < 3:

        user_choice = get_user_choice()
        system_choice = get_system_choice()
        winner = check_win(user_choice, system_choice)

        if winner == user_choice:
            msg = "You Win"
            result['user'] += 1
        elif winner == system_choice:
            msg = "You Lose"
            result['system'] += 1
        else:
            msg = "Draw!!"
        print("@" * 30)
        print(f"User: '{user_choice}'\t System: '{system_choice}'\t "
              f"Result: {msg}")
        print("@" * 30)
        print("--", f"User: {result['user']}", "--" * 5,
              f"System: {result['system']}", "--")
        print("@" * 30)
    update_table(result)
    play_again()


@timer
def play():
    play_one_hand()


if __name__ == '__main__':
    play()
