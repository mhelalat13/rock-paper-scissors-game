# Rock, Paper, Scissors game!

You can play with the computer by choosing one of the modes (rock, paper, scissors). The frequency of the game is based on the hands of the game. (Each hand consists of 3 rounds)

## How to run

run main.py to play. Enjoy:)

## Contribution
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License
[MIT](https://choosealicense.com/licenses/mit/)
